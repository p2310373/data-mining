# Data mining


Projet de data mining avec un dataset sur les transactions d'un supermarché.

Ce projet contient 3 documents :

- Un fichier .ipynb
- Un dataset CSV
- Un rapport PDF

Les trois étudiants qui ont travaillé sur ce projet sont :

- Prioux Léo-Paul p2310373
- Drici Mohamed Islam p2217699
- Bettahar Walid p2415589